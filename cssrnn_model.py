# -*- coding: utf-8 -*-

import tensorflow as tf

from model_config import ModelConfig
from rnn_model import RNNTrajModel


class CSSRNNTrajModel(RNNTrajModel):

    def __init__(self, config: ModelConfig):
        super(CSSRNNTrajModel, self).__init__(config)

    def build_model(self, queue: tf.PaddingFIFOQueue, batch_size, global_step):
        dequeue_dict = queue.dequeue_many(batch_size)
        inputs = dequeue_dict['inputs']
        labels = dequeue_dict['adj_labels']
        options = dequeue_dict['options']
        masks = dequeue_dict['masks']
        lens = dequeue_dict['lens']
        dests = dequeue_dict['dests']
        embeded_inputs = self.build_input(inputs, dests)
        rnn_outputs, train_final_state = self.rnn(batch_size, embeded_inputs, lens)
        outputs = self.build_css_outputs(rnn_outputs, options, batch_size)
        loss = self.build_loss(outputs, labels, masks, batch_size)
        acc = self.build_acc(outputs, labels, masks)
        total_weights = tf.reduce_sum(masks)
        train_loss_summary = tf.summary.scalar(name="train_loss_summary", tensor=loss)
        valid_loss_summary = tf.summary.scalar(name="valid_loss_summary", tensor=loss)
        train_acc_summary = tf.summary.scalar(name="train_acc_summary", tensor=acc)
        valid_acc_summary = tf.summary.scalar(name="valid_acc_summary", tensor=acc)
        train_summary = tf.summary.merge([train_loss_summary, train_acc_summary])
        valid_summary = tf.summary.merge([valid_loss_summary, valid_acc_summary])
        saver = tf.train.Saver(tf.global_variables())
        train_op, optimizer_init_op = self.build_optimizer(loss, global_step)
        return loss, acc, total_weights, train_summary, valid_summary, train_op, saver, optimizer_init_op

    def build_css_outputs(self, rnn_outputs, options, batch_size):
        config = self.config
        rnn_outputs = tf.reshape(rnn_outputs, [batch_size, -1, 1, config.hidden_dim])
        w = tf.nn.embedding_lookup(self.output_w, options)
        b = tf.nn.embedding_lookup(self.output_b, options)
        return tf.add(tf.reshape(tf.matmul(rnn_outputs, w, transpose_b=True), [batch_size, -1, config.max_adj]), b)
