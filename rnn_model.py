# -*- coding: utf-8 -*-

import math

import tensorflow as tf

from model_config import ModelConfig


class RNNTrajModel(object):

    def __init__(
            self,
            config: ModelConfig):
        self.config = config

    def construct(
            self,
            train_queue: tf.PaddingFIFOQueue,
            valid_queue: tf.PaddingFIFOQueue,
            test_queue: tf.PaddingFIFOQueue,
            global_step):
        config = self.config
        self.define_parameters()

        self.select_queue = tf.placeholder(dtype=tf.int64)
        queue = tf.QueueBase.from_list(self.select_queue, [train_queue, valid_queue, test_queue])

        self.loss, \
        self.acc, \
        self.total_weights, \
        self.train_summary, \
        self.valid_summary, \
        self.train_op, \
        self.saver, \
        self.optimizer_init_op = self.build_model(
            queue,
            config.batch_size,
            global_step)

    def build_model(self, queue: tf.PaddingFIFOQueue, batch_size, global_step):
        dequeue_dict = queue.dequeue_many(batch_size)
        inputs = dequeue_dict['inputs']
        labels = dequeue_dict['labels']
        masks = dequeue_dict['masks']
        lens = dequeue_dict['lens']
        dests = dequeue_dict['dests']
        embeded_inputs = self.build_input(inputs, dests)
        rnn_outputs, train_final_state = self.rnn(batch_size, embeded_inputs, lens)
        outputs = self.build_outputs(rnn_outputs, batch_size)
        loss = self.build_loss(outputs, labels, masks, batch_size)
        acc = self.build_acc(outputs, labels, masks)
        total_weights = tf.reduce_sum(masks)
        train_loss_summary = tf.summary.scalar(name="train_loss_summary", tensor=loss)
        valid_loss_summary = tf.summary.scalar(name="valid_loss_summary", tensor=loss)
        train_acc_summary = tf.summary.scalar(name="train_acc_summary", tensor=acc)
        valid_acc_summary = tf.summary.scalar(name="valid_acc_summary", tensor=acc)
        train_summary = tf.summary.merge([train_loss_summary, train_acc_summary])
        valid_summary = tf.summary.merge([valid_loss_summary, valid_acc_summary])
        saver = tf.train.Saver(tf.global_variables())
        train_op, optimizer_init_op = self.build_optimizer(loss, global_step)
        return loss, acc, total_weights, train_summary, valid_summary, train_op, saver, optimizer_init_op

    def define_parameters(self):
        config = self.config

        self.define_embeddings()
        self.output_keep_prob = tf.placeholder(dtype=config.float_type, shape=[], name='output_keep_prob')
        self.define_rnn_cell()
        self.define_output_parameters()

    def define_embeddings(self):
        config = self.config

        self.embeddings = tf.Variable(
            tf.random_uniform([config.vocabulary_size, config.embedding_size], -1, 1),
            name='embeddings')
        if config.embeddings_loading_path != '':
            self.embeddings_saver = tf.train.Saver({'embeddings': self.embeddings})
        if not config.embedding_trainable:
            self.embeddings = tf.stop_gradient(self.embeddings)

    def define_rnn_cell(self):
        config = self.config

        cell = {
            'basic': lambda: tf.contrib.rnn.BasicRNNCell(config.hidden_dim),
            'lstm': lambda: tf.contrib.rnn.BasicLSTMCell(config.hidden_dim),
            'gru': lambda: tf.contrib.rnn.GRUCell(config.hidden_dim),
        }[config.cell_type]
        if config.output_keep_prob < 1:
            cell = lambda c=cell: tf.contrib.rnn.DropoutWrapper(c(), output_keep_prob=self.output_keep_prob)
        if config.num_layers > 1:
            cell = lambda c=cell: tf.contrib.rnn.MultiRNNCell([c() for _ in range(config.num_layers)])
        self.cell = cell()

    def define_output_parameters(self):
        config = self.config

        self.output_w = tf.Variable(
            tf.truncated_normal(
                [config.vocabulary_size, config.hidden_dim],
                stddev=1.0 / math.sqrt(config.hidden_dim)),
            'output_w')
        self.output_b = tf.Variable(
            tf.zeros([config.vocabulary_size]),
            'output_b')

    def load_embeddings(self, sess):
        config = self.config
        if config.embeddings_loading_path != '':
            self.embeddings_saver.restore(sess, config.embeddings_loading_path)

    def rnn(self, batch_size, inputs, lens):
        config = self.config
        cell = self.cell

        initial_state = cell.zero_state(batch_size=batch_size, dtype=config.float_type)

        return tf.nn.dynamic_rnn(
            cell,
            inputs,
            initial_state=initial_state,
            sequence_length=lens)

    def build_input(self, inputs, dests):
        config = self.config

        embed = tf.nn.embedding_lookup(self.embeddings, inputs)
        if config.given_dests:
            dests = tf.nn.embedding_lookup(self.embeddings, dests)
            return tf.concat([embed, dests], axis=2)
        else:
            return embed

    def build_outputs(self, rnn_outputs, batch_size):
        config = self.config
        flat_rnn_outputs = tf.reshape(rnn_outputs, [-1, config.hidden_dim])
        flat_output_logits = tf.add(tf.matmul(flat_rnn_outputs, self.output_w, transpose_b=True), self.output_b)
        return tf.reshape(flat_output_logits, [batch_size, -1, config.vocabulary_size])

    def build_loss(self, output_logits, labels, masks,  batch_size):
        return tf.reduce_sum(tf.contrib.seq2seq.sequence_loss(
            output_logits,
            labels,
            masks,
            average_across_timesteps=False,
            average_across_batch=False,
            name='sequence_loss')) / batch_size

    def build_acc(self, output_logits, labels, masks):
        predictions = tf.argmax(output_logits, axis=2)
        return tf.contrib.metrics.accuracy(predictions, labels, weights=masks)

    def build_optimizer(self, loss, global_step):
        config = self.config
        params = tf.trainable_variables()
        old_vars = set(tf.global_variables())
        optimizer = tf.train.AdamOptimizer(learning_rate=config.lr)
        grads, vars = zip(*optimizer.compute_gradients(loss, params))
        clipped_grads, norm = tf.clip_by_global_norm(grads, config.max_graident_norm)
        train_op = optimizer.apply_gradients(zip(clipped_grads, params), global_step=global_step)
        optimizer_init_op = tf.variables_initializer(set(tf.global_variables()) - old_vars)
        return train_op, optimizer_init_op
