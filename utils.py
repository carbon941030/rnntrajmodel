# -*- coding: utf-8 -*-
from typing import List

import progressbar


def line2int_list(line: str) -> List[int]:
    line = line.split(',')
    line = map(lambda x: x.strip(), line)
    line = filter(lambda x: x != '', line)
    line = map(lambda x: int(x), line)
    return list(line)


def get_progress_bar(total, messages):
    widgets = ['|', progressbar.Percentage(), '|']
    widgets += ['(', progressbar.Counter() , ' of %d)'%(total)]
    widgets += [progressbar.Bar()]
    widgets += [progressbar.Timer(), '|']
    widgets += [progressbar.ETA(), '|']
    for message in messages:
        widgets += [progressbar.DynamicMessage(message), '|']
    return progressbar.ProgressBar(max_value=total, widgets=widgets)
