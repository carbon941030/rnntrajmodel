# -*- coding: utf-8 -*-

import math

import tensorflow as tf

from model_config import ModelConfig
from rnn_model import RNNTrajModel


class LPIRNNTrajModel(RNNTrajModel):

    def __init__(self, config:ModelConfig):
        super(LPIRNNTrajModel, self).__init__(config)

    def define_output_parameters(self):
        config = self.config

        self.middle_w = tf.Variable(
            tf.truncated_normal(
                [config.hidden_dim, config.lpirnn_classifier_dim],
                stddev=1.0 / math.sqrt(config.hidden_dim)),
            name='middle_w')
        self.middle_b = tf.Variable(
            tf.zeros([config.lpirnn_classifier_dim]),
            name='middle_b')

        self.output_w = tf.Variable(
            tf.truncated_normal(
                [config.num_lpirnn_classifier, config.lpirnn_classifier_dim, config.max_adj],
                stddev=1.0 / math.sqrt(config.lpirnn_classifier_dim)),
            name='output_w')
        self.output_b = tf.Variable(
            tf.zeros([config.num_lpirnn_classifier, config.max_adj]),
            name='output_b')

    def build_model(self, queue: tf.PaddingFIFOQueue, batch_size, global_step):
        dequeue_dict = queue.dequeue_many(batch_size)
        inputs = dequeue_dict['inputs']
        labels = dequeue_dict['adj_labels']
        classifier_ids = dequeue_dict['classifier_ids']
        masks = dequeue_dict['masks']
        lens = dequeue_dict['lens']
        dests = dequeue_dict['dests']
        embeded_inputs = self.build_input(inputs, dests)
        rnn_outputs, train_final_state = self.rnn(batch_size, embeded_inputs, lens)
        outputs = self.build_lpi_outputs(rnn_outputs, classifier_ids, batch_size)
        loss = self.build_loss(outputs, labels, masks, batch_size)
        acc = self.build_acc(outputs, labels, masks)
        total_weights = tf.reduce_sum(masks)
        train_loss_summary = tf.summary.scalar(name="train_loss_summary", tensor=loss)
        valid_loss_summary = tf.summary.scalar(name="valid_loss_summary", tensor=loss)
        train_acc_summary = tf.summary.scalar(name="train_acc_summary", tensor=acc)
        valid_acc_summary = tf.summary.scalar(name="valid_acc_summary", tensor=acc)
        train_summary = tf.summary.merge([train_loss_summary, train_acc_summary])
        valid_summary = tf.summary.merge([valid_loss_summary, valid_acc_summary])
        saver = tf.train.Saver(tf.global_variables())
        train_op, optimizer_init_op = self.build_optimizer(loss, global_step)
        return loss, acc, total_weights, train_summary, valid_summary, train_op, saver, optimizer_init_op

    def build_lpi_outputs(self, rnn_outputs, classifier_ids, batch_size):
        config = self.config

        flat_rnn_outputs = tf.reshape(rnn_outputs, [-1, config.hidden_dim])
        flat_middle_outputs = tf.add(tf.matmul(flat_rnn_outputs, self.middle_w), self.middle_b)
        middle_outputs = tf.reshape(flat_middle_outputs, [batch_size, -1, 1, config.lpirnn_classifier_dim])
        activation = {
            'identity': tf.identity,
            'tanh': tf.tanh
        }[config.lpirnn_activation]
        middle_outputs = activation(middle_outputs)
        w = tf.nn.embedding_lookup(self.output_w, classifier_ids)
        b = tf.nn.embedding_lookup(self.output_b, classifier_ids)
        return tf.add(tf.reshape(tf.matmul(middle_outputs, w), [batch_size, -1, config.max_adj]), b)
