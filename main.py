#!/bin/python3
# -*- coding: utf-8 -*-

import configparser
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='1'
import tensorflow as tf

from model_config import ModelConfig
from model_trainer import ModelTrainer

FLAGS = tf.flags.FLAGS

tf.flags.DEFINE_string('config_path', '',
                       'The path of the config file.')


def main(_):
    config_file = configparser.ConfigParser()
    config_file.read(FLAGS.config_path)
    model_base_config = config_file['model_base']
    training_config = config_file['training']
    file_path_config = config_file['file_path']
    reader_config = config_file['reader']
    lpirnn_config = config_file['lpirnn']
    echrnn_config = config_file['echrnn']

    seq_len = model_base_config.getint('seq_len')
    if seq_len < 0:
        seq_len = None
    batch_size = model_base_config.getint('batch_size')
    train_samples = reader_config.getint('train_samples')
    valid_samples = reader_config.getint('valid_samples')
    test_samples = reader_config.getint('test_samples')
    train_samples = train_samples - train_samples % batch_size
    valid_samples = valid_samples - valid_samples % batch_size
    test_samples = test_samples - test_samples % batch_size

    config = ModelConfig(
        cell_type=model_base_config['cell_type'],
        hidden_dim=model_base_config.getint('hidden_dim'),
        output_keep_prob=training_config.getfloat('output_keep_prob'),
        num_layers=model_base_config.getint('num_layers'),
        batch_size=model_base_config.getint('batch_size'),
        float_type={
            'float16': tf.float16,
            'float32': tf.float32,
            'float64': tf.float64
        }[model_base_config['float_type']],
        seq_len=seq_len,
        vocabulary_size=model_base_config.getint('vocabulary_size'),
        embedding_size=model_base_config.getint('embedding_size'),
        embedding_trainable=model_base_config.getboolean('embedding_trainable'),
        given_dests=model_base_config.getboolean('given_dests'),
        max_adj=model_base_config.getint('max_adj'),
        num_lpirnn_classifier=lpirnn_config.getint('num_lpirnn_classifier'),
        lpirnn_classifier_dim=lpirnn_config.getint('lpirnn_classifier_dim'),
        lpirnn_activation=lpirnn_config['lpirnn_activation'],
        echrnn_num_layer=echrnn_config.getint('echrnn_num_layer'),
        echrnn_classifier_dim=echrnn_config.getint('echrnn_classifier_dim'),
        echrnn_activation=echrnn_config.get('echrnn_activation'),
        queue_capacity=reader_config.getint('queue_capacity'),
        rs_queue_capacity=reader_config.getint('rs_queue_capacity'),
        train_samples=train_samples,
        train_batches=train_samples // batch_size,
        valid_samples=valid_samples,
        valid_batches=valid_samples // batch_size,
        test_samples=test_samples,
        test_batches=test_samples // batch_size,
        gpu_fraction=training_config.getfloat('gpu_fraction'),
        lr=training_config.getfloat('lr'),
        max_graident_norm=training_config.getfloat('max_gradient_norm'),
        checkpoint_duration=training_config.getint('checkpoint_duration'),
        map_path=file_path_config['map_path'],
        model_type=model_base_config['model_type'],
        embeddings_loading_path=file_path_config.get('embeddings_loading_path', fallback=''),
        train_file_name=file_path_config['train_file_name'],
        valid_file_name=file_path_config['valid_file_name'],
        test_file_name=file_path_config['test_file_name'],
        stop_control_filename=file_path_config['stop_control_filename'],
        log_dir=file_path_config['log_dir']
    )

    trainer = ModelTrainer(config)

    trainer.train()

if __name__ == '__main__':
    tf.app.run()
