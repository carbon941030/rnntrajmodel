# -*- coding: utf-8 -*-

import numpy as np
import tensorflow as tf

from model_config import ModelConfig
from utils import line2int_list
from road_map import RoadMap


class Reader(object):

    def __init__(self, config: ModelConfig, file_name, samples, r_map: RoadMap):
        self.config = config
        self.file_name = file_name
        self.samples = samples
        self.r_map = r_map

        self.rs_queue = tf.RandomShuffleQueue(
            capacity=config.rs_queue_capacity,
            min_after_dequeue=config.rs_queue_capacity - config.queue_capacity,
            dtypes=[tf.int64, tf.int64, tf.int64, tf.int64, tf.int64, tf.int64, tf.int64, config.float_type],
            names=['lens', 'inputs', 'labels', 'dests', 'options', 'adj_labels', 'classifier_ids', 'masks'])
        self.queue = tf.PaddingFIFOQueue(
            capacity=config.queue_capacity,
            dtypes=[tf.int64, tf.int64, tf.int64, tf.int64, tf.int64, tf.int64, tf.int64, config.float_type],
            shapes=[[], [None], [None], [None], [None, config.max_adj], [None], [None], [None]],
        names = ['lens', 'inputs', 'labels', 'dests', 'options', 'adj_labels', 'classifier_ids', 'masks'],
            name='queue')
        self.enqueue_len = tf.placeholder(dtype=tf.int64, name='enqueue_len')
        self.enqueue_input = tf.placeholder(dtype=tf.int64, name='enqueue_input')
        self.enqueue_label = tf.placeholder(dtype=tf.int64, name='enqueue_label')
        self.enqueue_dest = tf.placeholder(dtype=tf.int64, name='enqueue_dest')
        self.enqueue_options = tf.placeholder(dtype=tf.int64, name='enqueue_options')
        self.enqueue_adj_label = tf.placeholder(dtype=tf.int64, name='enqueue_adj_label')
        self.enqueue_classifier_id = tf.placeholder(dtype=tf.int64, name='enqueue_classifier_id')
        self.enqueue_mask = tf.placeholder(dtype=config.float_type, name='enqueue_mask')
        self.rs_enqueue_op = self.rs_queue.enqueue(
            {
                'lens': self.enqueue_len,
                'inputs': self.enqueue_input,
                'labels': self.enqueue_label,
                'dests': self.enqueue_dest,
                'options': self.enqueue_options,
                'adj_labels': self.enqueue_adj_label,
                'classifier_ids': self.enqueue_classifier_id,
                'masks': self.enqueue_mask
            })
        self.enqueue_op = self.queue.enqueue(self.rs_queue.dequeue(), name='enqueue_op')
        self.qr = tf.train.QueueRunner(self.queue, [self.enqueue_op])
        tf.train.queue_runner.add_queue_runner(self.qr)

    def load_and_enqueue(self, sess: tf.Session):
        config = self.config
        r_map = self.r_map
        with open(self.file_name, 'r') as f:
            traj_count = 0
            for line in f:
                traj = line2int_list(line)
                if config.seq_len is not None:
                    if len(traj) > config.seq_len + 1 or len(traj) < 2:
                        continue
                traj_count += 1
                inputs = np.array(traj[:-1])
                labels = np.array(traj[1:])
                options = [r_map.adj_edge_ids[r_map.edges[u][1]] for u, v in zip(traj[:-1], traj[1:])]
                options = np.array(options)
                adj_label = [r_map.adj_id_dict[(r_map.edges[u][1], v)] for u, v in zip(traj[:-1], traj[1:])]
                adj_label = np.array(adj_label)
                classifier_id = [r_map.edges[u][1] for u in traj[:-1]]
                masks = np.ones([len(traj) - 1], dtype=np.float32)
                length = len(traj) - 1
                dest = np.array(traj[-1:] * (len(traj) - 1))
                sess.run(
                    self.rs_enqueue_op,
                    feed_dict={
                        self.enqueue_len: length,
                        self.enqueue_input: inputs,
                        self.enqueue_label: labels,
                        self.enqueue_dest: dest,
                        self.enqueue_options: options,
                        self.enqueue_adj_label: adj_label,
                        self.enqueue_classifier_id: classifier_id,
                        self.enqueue_mask: masks
                    })
                if traj_count >= self.samples:
                    break
