# -*- coding: utf-8 -*-

import math

import tensorflow as tf

from model_config import ModelConfig
from rnn_model import RNNTrajModel


class ECHRNNTrajModel(RNNTrajModel):

    def __init__(self, config: ModelConfig):
        super(ECHRNNTrajModel, self).__init__(config)

    @staticmethod
    def full_connected_weights(input_dim, output_dim, name_prefix):
        w = tf.Variable(
                tf.truncated_normal(
                    [input_dim, output_dim],
                    stddev=1.0 / math.sqrt(input_dim)),
                name=name_prefix+'_w')
        b = tf.Variable(
                tf.zeros([output_dim]),
                name=name_prefix+'_b')
        return w, b

    def define_output_parameters(self):
        config = self.config

        self.middle_w = []
        self.middle_b = []
        dim = config.hidden_dim + config.embedding_size
        for layers in range(config.echrnn_num_layer):
            w, b = ECHRNNTrajModel.full_connected_weights(dim, config.echrnn_classifier_dim, 'middle')
            self.middle_w.append(w)
            self.middle_b.append(b)
            dim = config.echrnn_classifier_dim
        self.output_w, self.output_b = ECHRNNTrajModel.full_connected_weights(dim, 1, 'output')

    def build_model(self, queue: tf.PaddingFIFOQueue, batch_size, global_step):
        dequeue_dict = queue.dequeue_many(batch_size)
        inputs = dequeue_dict['inputs']
        labels = dequeue_dict['adj_labels']
        options = dequeue_dict['options']
        masks = dequeue_dict['masks']
        lens = dequeue_dict['lens']
        dests = dequeue_dict['dests']
        embeded_inputs = self.build_input(inputs, dests)
        rnn_outputs, train_final_state = self.rnn(batch_size, embeded_inputs, lens)
        outputs = self.build_ech_outputs(rnn_outputs, options, batch_size)
        loss = self.build_loss(outputs, labels, masks, batch_size)
        acc = self.build_acc(outputs, labels, masks)
        total_weights = tf.reduce_sum(masks)
        train_loss_summary = tf.summary.scalar(name="train_loss_summary", tensor=loss)
        valid_loss_summary = tf.summary.scalar(name="valid_loss_summary", tensor=loss)
        train_acc_summary = tf.summary.scalar(name="train_acc_summary", tensor=acc)
        valid_acc_summary = tf.summary.scalar(name="valid_acc_summary", tensor=acc)
        train_summary = tf.summary.merge([train_loss_summary, train_acc_summary])
        valid_summary = tf.summary.merge([valid_loss_summary, valid_acc_summary])
        saver = tf.train.Saver(tf.global_variables())
        train_op, optimizer_init_op = self.build_optimizer(loss, global_step)
        return loss, acc, total_weights, train_summary, valid_summary, train_op, saver, optimizer_init_op

    def build_ech_outputs(self, rnn_outputs, options, batch_size):
        config = self.config

        opt_emb = tf.nn.embedding_lookup(self.embeddings, options)
        hidden = tf.reshape(rnn_outputs, [batch_size, -1, 1, config.hidden_dim])
        hidden = tf.tile(hidden, [1, 1, config.max_adj, 1])
        middle = tf.concat([opt_emb, hidden], axis=3)
        middle = tf.reshape(middle, [-1, config.hidden_dim + config.embedding_size])
        for w, b in zip(self.middle_w, self.middle_b):
            activation = {
                'identity': tf.identity,
                'tanh': tf.tanh
            }[config.echrnn_activation]
            middle = activation(tf.add(tf.matmul(middle, w), b))
        logits = tf.add(tf.matmul(middle, self.output_w), self.output_b)
        return tf.reshape(logits, [batch_size, -1, config.max_adj])
