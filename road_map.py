# -*- coding: utf-8 -*-

from collections import defaultdict
import os
import re


class RoadMap(object):

    def __init__(self, pad_id, max_adj=6):
        self.pad_id = pad_id
        self.max_adj = max_adj
        self.nodes = {}
        self.edges = {}
        self.adj_edge_ids = defaultdict(list)
        self.adj_id_dict = {}

    def open(self, map_path):
        # Load nodeOSM.txt
        with open(os.path.join(map_path, 'nodeOSM.txt')) as node_file:
            for line in node_file:
                values = re.split('[\n\t ]', line)
                node_id, lat, lon = int(values[0]), float(values[1]), float(values[2])
                self.nodes[node_id] = (lat, lon)

        # Load edgeOSM.txt
        with open(os.path.join(map_path, 'edgeOSM.txt')) as edge_file:
            for line in edge_file:
                values = re.split('[\n\t ]', line)
                edge_id, start_node_id, end_node_id = int(values[0]), int(values[1]), int(values[2])
                # TODO: Load shape information of edge
                self.edges[edge_id] = (start_node_id, end_node_id)
                self.adj_id_dict[(start_node_id, edge_id)] = len(self.adj_edge_ids[start_node_id])
                self.adj_edge_ids[start_node_id].append(edge_id)

        for edge_id, adj_ids in self.adj_edge_ids.items():
            while len(adj_ids) < self.max_adj:
                adj_ids.append(self.pad_id)
