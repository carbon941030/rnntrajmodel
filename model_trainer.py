# -*- coding: utf-8 -*-

from collections import namedtuple
import os
import threading

import tensorflow as tf

from cssrnn_model import CSSRNNTrajModel
from echrnn_model import ECHRNNTrajModel
from lpirnn_model import LPIRNNTrajModel
from slirnn_model import SLIRNNTrajModel
from model_config import ModelConfig
from reader import Reader
from rnn_model import RNNTrajModel
from road_map import RoadMap
from utils import get_progress_bar


class ModelTrainer(object):
    def __init__(self, config: ModelConfig):
        self.config = config

        self.r_map = RoadMap(config.vocabulary_size - 1)
        self.r_map.open(config.map_path)

        self.train_reader = Reader(config, config.train_file_name, config.train_samples, self.r_map)
        self.valid_reader = Reader(config, config.valid_file_name, config.valid_samples, self.r_map)
        self.test_reader = Reader(config, config.test_file_name, config.test_samples, self.r_map)

        self.global_step = tf.Variable(0, name='global_step', trainable=False)
        self.increase_global_step = tf.assign_add(self.global_step, tf.constant(1))

        with tf.name_scope('epoch_summary'):
            self.add_epoch_summary()

        Model = {
            'basic': RNNTrajModel,
            'cssrnn': CSSRNNTrajModel,
            'lpirnn': LPIRNNTrajModel,
            'echrnn': ECHRNNTrajModel,
            'slirnn': SLIRNNTrajModel,
        }[config.model_type]
        self.model = Model(config)
        self.model.construct(
            self.train_reader.queue,
            self.valid_reader.queue,
            self.test_reader.queue,
            self.global_step)

        checkpoint_duration = config.checkpoint_duration
        if checkpoint_duration < 0:
            checkpoint_duration = 0

        self.sv = tf.train.Supervisor(
            logdir=config.log_dir,
            global_step=self.global_step,
            summary_op=None,
            saver = self.model.saver,
            save_model_secs=checkpoint_duration,
            init_fn=self.init_fn,
            local_init_op=self.model.optimizer_init_op)

    def add_epoch_summary(self):
        self.epoch_train_loss = tf.placeholder(dtype=tf.float64)
        self.epoch_train_acc = tf.placeholder(dtype=tf.float64)
        self.epoch_valid_loss = tf.placeholder(dtype=tf.float64)
        self.epoch_valid_acc = tf.placeholder(dtype=tf.float64)
        self.epoch_test_loss = tf.placeholder(dtype=tf.float64)
        self.epoch_test_acc = tf.placeholder(dtype=tf.float64)
        self.epoch_train_summary = tf.summary.merge([
            tf.summary.scalar('epoch_train_loss', self.epoch_train_loss),
            tf.summary.scalar('epoch_train_acc', self.epoch_train_acc)
        ])
        self.epoch_valid_summary = tf.summary.merge([
            tf.summary.scalar('epoch_valid_loss', self.epoch_valid_loss),
            tf.summary.scalar('epoch_valid_acc', self.epoch_valid_acc)
        ])
        self.epoch_test_summary = tf.summary.merge([
            tf.summary.scalar('epoch_test_loss', self.epoch_test_loss),
            tf.summary.scalar('epoch_test_acc', self.epoch_test_acc)
        ])

    @staticmethod
    def should_stop_by_outerfile(filename) -> bool:
        should_stop = False
        with open(filename, "r") as f:
            command = f.readline()
            if command == "STOP":
                should_stop = True
        return should_stop

    def keep_loading(self, reader, sess):
        config = self.config
        while not ModelTrainer.should_stop_by_outerfile(config.stop_control_filename):
            reader.load_and_enqueue(sess)

    def init_fn(self, sess):
        self.model.load_embeddings(sess)

    def train(self):
        config = self.config

        print('Start training')

        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=config.gpu_fraction)

        epoch_count = 0
        with self.sv.managed_session(config=tf.ConfigProto(gpu_options=gpu_options)) as sess:
            train_load_thread = threading.Thread(
                target=self.keep_loading,
                args=(self.train_reader, sess))
            train_load_thread.start()
            valid_load_thread = threading.Thread(
                target=self.keep_loading,
                args=(self.valid_reader, sess))
            valid_load_thread.start()
            test_load_thread = threading.Thread(
                target=self.keep_loading,
                args=(self.test_reader, sess))
            test_load_thread.start()

            while not ModelTrainer.should_stop_by_outerfile(config.stop_control_filename):
                epoch_count += 1
                print('Start epoch %d:' % epoch_count)

                train_loss = 0.0
                total_correct = 0.0
                total_weights = 0.0
                with get_progress_bar(config.train_batches, ['loss', 'acc']) as bar:
                    for train_step in range(config.train_batches):
                        feed_dict = {
                            self.model.output_keep_prob: config.output_keep_prob,
                            self.model.select_queue: 0
                        }
                        fetch_list = [
                            self.model.train_op,
                            self.model.loss,
                            self.model.acc,
                            self.model.total_weights,
                        ]
                        _, step_loss, step_acc, step_weights = sess.run(
                            fetch_list,
                            feed_dict=feed_dict)
                        train_loss += step_loss
                        total_weights += step_weights
                        total_correct += step_acc * step_weights
                        avg_loss = train_loss / (train_step + 1)
                        avg_acc = total_correct / total_weights
                        bar.update(train_step + 1, loss=avg_loss, acc=avg_acc)
                train_loss /= config.train_batches
                train_acc = total_correct / total_weights
                print('Epoch %d, training phase ended, training loss: %f, acc: %f'
                      % (epoch_count, train_loss, train_acc))
                self.sv.summary_computed(sess, sess.run(
                    self.epoch_train_summary,
                    feed_dict={
                        self.epoch_train_loss: train_loss,
                        self.epoch_train_acc: train_acc
                    }))

                valid_loss = 0.0
                total_correct = 0.0
                total_weights = 0.0
                with get_progress_bar(config.valid_batches, ['loss', 'acc']) as bar:
                    for valid_step in range(config.valid_batches):
                        feed_dict = {
                            self.model.output_keep_prob: 1.0,
                            self.model.select_queue: 1
                        }
                        fetch_list = [
                            self.model.loss,
                            self.model.acc,
                            self.model.total_weights,
                            self.increase_global_step
                        ]
                        step_loss, step_acc, step_weights, _ = sess.run(
                            fetch_list,
                            feed_dict=feed_dict)
                        valid_loss += step_loss
                        total_weights += step_weights
                        total_correct += step_acc * step_weights
                        avg_loss = valid_loss / (valid_step + 1)
                        avg_acc = total_correct / total_weights
                        bar.update(valid_step + 1, loss=avg_loss, acc=avg_acc)
                valid_loss /= config.valid_batches
                valid_acc = total_correct / total_weights
                print('Epoch %d, validation phase ended, validation loss: %f, acc: %f'
                      % (epoch_count, valid_loss, valid_acc))
                self.sv.summary_computed(sess, sess.run(
                    self.epoch_valid_summary,
                    feed_dict={
                        self.epoch_valid_loss: valid_loss,
                        self.epoch_valid_acc: valid_acc
                    }))

                test_loss = 0.0
                total_correct = 0.0
                total_weights = 0.0
                with get_progress_bar(config.test_batches, ['loss', 'acc']) as bar:
                    for test_step in range(config.test_batches):
                        feed_dict = {
                            self.model.output_keep_prob: 1.0,
                            self.model.select_queue: 2
                        }
                        fetch_list = [
                            self.model.loss,
                            self.model.acc,
                            self.model.total_weights,
                            self.increase_global_step
                        ]
                        step_loss, step_acc, step_weights, _ = sess.run(
                            fetch_list,
                            feed_dict=feed_dict)
                        test_loss += step_loss
                        total_weights += step_weights
                        total_correct += step_acc * step_weights
                        avg_loss = test_loss / (test_step + 1)
                        avg_acc = total_correct / total_weights
                        bar.update(test_step + 1, loss=avg_loss, acc=avg_acc)
                test_loss /= config.test_batches
                test_acc = total_correct / total_weights
                print('Epoch %d, test phase ended, test loss: %f, acc: %f\n'
                      % (epoch_count, test_loss, test_acc))
                self.sv.summary_computed(sess, sess.run(
                    self.epoch_test_summary,
                    feed_dict={
                        self.epoch_test_loss: test_loss,
                        self.epoch_test_acc: test_acc
                    }))

                if config.checkpoint_duration == 0:
                    self.model.saver.save(
                        sess,
                        os.path.join(config.log_dir, 'model.ckpt'),
                        global_step=self.global_step)
